package fluentconditionals;

import java.util.function.Supplier;

/**
 * Created by marek on 24.12.2017.
 */
interface OrElseThrow<T> {
    T orElseThrow(Supplier<RuntimeException> r);

    default T orElseThrowE(RuntimeException r){
        return orElseThrow(() -> r);
    }
}
