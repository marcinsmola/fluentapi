package fluentconditionals;

import java.util.function.Supplier;

class ReturnObjectImpl<T> implements ReturnObject<T> {
    private T value;
    private Supplier<Boolean> condition;

    ReturnObjectImpl(T returnObj, Supplier<Boolean> condition) {
        this.value = returnObj;
        this.condition = condition;
    }

    public T orElse(Supplier<T> object) {
        if (!condition.get()) {
            return object.get();
        }
        return value;
    }

    public T orElseThrow(Supplier<RuntimeException> exceptionSupplier) {
        if (!condition.get()) {
            throw exceptionSupplier.get();
        }
        return value;
    }
}