package fluentconditionals;

import java.util.function.Supplier;

/**
 * Created by marek on 16.12.2017.
 */
interface Conditional {

    Conditional then(Runnable runnable);

    Conditional orElse(Runnable runnable);

    <T> ReturnObject<T> thenReturn(Supplier<T> object);

    default <T> ReturnObject<T> thenReturn(T object){
        return thenReturn(() -> object);
    }

    void orElseThrow(Supplier<RuntimeException> r);

    default void orElseThrowE(RuntimeException r){
        orElseThrow(() -> r);
    }
}