package fluentconditionals;

import static fluentconditionals.FluentConditionals.*;

//Task 1
public class IfElse {

    public static void main(String[] args) {

        when(fluentconditionals.TestHelper.somethingIsTrue())
                .then(fluentconditionals.TestHelper::printBar)
                .orElse(fluentconditionals.TestHelper::printFoo);
        //'Bar' printed to console

        when(fluentconditionals.TestHelper::somethingIsTrue)
                .then(fluentconditionals.TestHelper::printBar)
                .orElse(fluentconditionals.TestHelper::printFoo);
        //'Bar' printed to console


        when(!fluentconditionals.TestHelper.somethingIsTrue())
                .then(fluentconditionals.TestHelper::printBar)
                .orElse(doNothing);
        //nothing printed to console
    }

}
