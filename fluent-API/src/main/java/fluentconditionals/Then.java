package fluentconditionals;

import java.util.function.Consumer;

/**
 * Created by marek on 24.12.2017.
 */
interface Then<T> extends ThenReturn {
    <V> OrElse<T, V> then(Consumer<T> consumer);
}
