package fluentconditionals;

import java.util.function.Supplier;

/**
 * Created by marek on 17.12.2017.
 */
public interface ReturnObject<T>  {
    T orElse(Supplier<T> object);

    default T orElse(T obj) {
        return orElse(() -> obj);
    }

    default T orElseThrowE(RuntimeException exception) {
        return orElseThrow(() -> exception);
    }

    T orElseThrow(Supplier<RuntimeException> exceptionSupplier);
}