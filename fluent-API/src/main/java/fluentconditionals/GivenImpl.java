package fluentconditionals;


import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by marek on 17.12.2017.
 */
class GivenImpl<T> implements Given<T> {

    private Supplier<Boolean> condition;
    private Supplier<T> msg;
    private Consumer<T> consumer;

    GivenImpl(Supplier<T> supplier) {
        this.msg = supplier;
    }

    @Override
    public ThenReturn<T> when(Supplier<Boolean> supplier) {
        this.condition = supplier;
        return new ThenReturnImpl(condition, msg);
    }

    @Override
    public ThenReturn<T> when(boolean value) {
        return when(() -> value);
    }

    @Override
    public OrElse<T> then(Consumer<T> consumer) {
        this.consumer = consumer;
    }

    @Override
    public T orElse(Consumer<T> consumer) {
        if(!condition.get()){
            consumer.accept(msg.get());
        }
        this.consumer.accept(msg.get());
    }

    @Override
    public T orElse(T val) {
        return val;
    }

    @Override
    public void orElseThrow(Supplier<RuntimeException> r) {
        throw r.get();
    }

    @Override
    public <T> OrElse<T> thenReturn(Supplier<T> supplier) {
        return new OrElseImpl(supplier);
    }
}