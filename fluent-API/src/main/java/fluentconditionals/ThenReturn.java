package fluentconditionals;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by marek on 24.12.2017.
 */
interface ThenReturn<T, V> extends OrElse<T, V> {
    <V> OrElse<T, V> thenReturn(Supplier<T> supplier);

    <V> OrElse<T, V> thenReturn(Function<T, V> function);
}
