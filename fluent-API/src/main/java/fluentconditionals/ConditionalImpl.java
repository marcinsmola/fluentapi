package fluentconditionals;

import java.util.function.Supplier;

/**
 * Created by marek on 17.12.2017.
 */
class ConditionalImpl implements Conditional {
    private Supplier<Boolean> condition;

    ConditionalImpl(Supplier<Boolean> condition) {
        this.condition = condition;
    }

    public Conditional then(Runnable runnable) {
        if(condition.get()){
            runnable.run();
        }
        return this;
    }

    public Conditional orElse(Runnable runnable){
        if(!condition.get()){
            runnable.run();
        }
        return this;
    }

    public <T> ReturnObject<T> thenReturn(Supplier<T> object) {
        return new ReturnObjectImpl<>(object.get(), condition);
    }

    public void orElseThrow(Supplier<RuntimeException> r) {
        if(!condition.get()){
            throw r.get();
        }
    }
}