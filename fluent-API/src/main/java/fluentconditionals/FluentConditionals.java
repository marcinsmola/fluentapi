package fluentconditionals;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by marek on 16.12.2017.
 */
class FluentConditionals<T> {
    static Runnable doNothing = () -> {};

    static <T> Consumer<T> doNothing(){
       return o -> {};
    }

    static Conditional when(Supplier<Boolean> supplier){
      return new ConditionalImpl(supplier);
    }

    static Conditional when(boolean value){
        return new ConditionalImpl(() -> value);
    }

    static <T> Given<T> given(Supplier<T> supplier) {
        return new GivenImpl<>(supplier);
    }

    static <T> Given<T> given(T val) {
        return given(() -> val);
    }
}