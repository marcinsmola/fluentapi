package fluentconditionals;

import java.util.function.Supplier;

/**
 * Created by marek on 17.12.2017.
 */
interface Given<T> {
    Then<T> when(Supplier<Boolean> supplier);

    Then<T> when(boolean value);

}