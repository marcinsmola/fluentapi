package fluentconditionals;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by marek on 24.12.2017.
 */
interface OrElse<T, V> extends OrElseThrow<T> {
    V orElse(Consumer<T> consumer);

    V orElse(Function<T, V> consumer);

    V orElse(T val);
}
