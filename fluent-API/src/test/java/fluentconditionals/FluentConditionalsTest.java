package fluentconditionals;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Supplier;

import static fluentconditionals.FluentConditionals.*;


/**
 * Created by marek on 16.12.2017.
 */
public class FluentConditionalsTest {

    private ByteArrayOutputStream outContent;

    @BeforeMethod
    public void setUp(){
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    //Task1
    @Test
    public void whenTrue_thenShouldExecute() throws Exception {
        //given
        Supplier<Boolean> supplier = () -> Boolean.TRUE;

        //when
        when(supplier).then(TestHelper::printBar).orElse(TestHelper::printFoo);

        //then
        Assert.assertTrue(outContent.toString().contains("Bar"));
    }

    @Test
    public void whenTrue_thenShouldExecute_byMethodReference() throws Exception {
        //when
        when(TestHelper::somethingIsTrue).then(TestHelper::printBar).orElse(TestHelper::printFoo);

        //then
        Assert.assertTrue(outContent.toString().contains("Bar"));
    }

    @Test
    public void whenTrue_thenDoNothing() throws Exception {
        //when
        when(!TestHelper.somethingIsTrue()).then(TestHelper::printBar).orElse(doNothing);

        //then
        Assert.assertTrue(outContent.toString().isEmpty());
    }


    //Task2
    @Test
    public void whenTrue_thenPrintBar() throws Exception {
        //when
        when(TestHelper::somethingIsTrue)
                .then(TestHelper::printBar)
                .orElseThrowE(new RuntimeException());

        //then
        Assert.assertTrue(outContent.toString().contains("Bar"));
    }


    @Test
    public void whenTrue_thenPrintBar_methodRefEx() throws Exception {
        //when
        when(TestHelper::somethingIsTrue)
                .then(TestHelper::printBar)
                .orElseThrow(RuntimeException::new);

        //then
        Assert.assertTrue(outContent.toString().contains("Bar"));
    }

    @Test
    public void whenTrue_thenPrintBar_methodRef() throws Exception {
        //when
        when(TestHelper::somethingIsTrue)
                .then(TestHelper::printBar)
                .orElseThrow(TestHelper::createException);

        //then
        Assert.assertTrue(outContent.toString().contains("Bar"));
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void whenFalse_thenThrowRE() throws Exception {
        //when-then
        when(!TestHelper.somethingIsTrue())
                .then(TestHelper::printFoo)
                .orElseThrow(TestHelper::createException);
    }

    //Task3
    @Test
    public void whenTrue_then_shouldReturn1000() throws Exception {
        //when
        int result1 = when(TestHelper::somethingIsTrue)
                .thenReturn(TestHelper::getHighNumber)
                .orElse(TestHelper::getLowNumber);

        //then
        Assert.assertEquals(result1 , 1000);
    }

    @Test
    public void whenFalse_then_shouldReturn0() throws Exception {
        //when
        int result2 = when(!TestHelper.somethingIsTrue())
                .thenReturn(TestHelper::getHighNumber)
                .orElse(0);

        //then
        Assert.assertEquals(result2 , 0);
    }

    //Task4

    @Test
    public void whenTrue_then_shouldReturn1() throws Exception {
        //when
        int result3 = when(TestHelper::somethingIsTrue)
                .thenReturn(TestHelper::getLowNumber)
                .orElseThrowE(new RuntimeException());
        System.out.println(result3);

        //then
        Assert.assertEquals(result3 , 1);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void whenFalse_thenThrowRE_withReturnTypeInt() throws Exception {
        //when-then
        int result4 = when(!TestHelper.somethingIsTrue())
                .thenReturn(TestHelper::getLowNumber)
                .orElseThrow(RuntimeException::new);
    }

    //Task5

    @Test
    public void whenTrue_then_shouldReturnYayString() throws Exception {
        //when
        String string =
                when(TestHelper::somethingIsTrue)
                        .thenReturn("Yay")
                        .orElse("Nah");
        //then
        Assert.assertEquals(string , "Yay");
    }

    @Test
    public void whenTrue_then_shouldReturnSomeClass() throws Exception {
        //when
        SomeClass customObject =
                when(TestHelper::somethingIsTrue)
                        .thenReturn(new SomeClass())
                        .orElse(SomeClass::new);
        //then
        Assert.assertNotNull(customObject);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void whenFalse_thenThrowRE_withReturnTypeSomeClass() throws Exception {
        //when-then
        SomeClass customObject2 =
                when(!TestHelper.somethingIsTrue())
                        .thenReturn(SomeClass::new)
                        .orElseThrow(RuntimeException::new);
    }

    //Task6

    @Test
    public void given_whenTrue_then_shouldPrintT() throws Exception {
        //when
        given("This")
                .when(true)
                .then(TestHelper::printFirstChar)
                .orElse(TestHelper::printLastChar);

        //then
        Assert.assertTrue(outContent.toString().contains("T"));
    }

    @Test
    public void given_whenTrue_then_shouldPrintA() throws Exception {
        //when
        given(TestHelper::getAString)
                .when(TestHelper::somethingIsTrue)
                .then(TestHelper::printFirstChar)
                .orElse(TestHelper::printLastChar);

        //then
        Assert.assertTrue(outContent.toString().contains("a"));
    }

    @Test
    public void given_whenFalse_then_shouldPrintNothing() throws Exception {
        //when
        given(TestHelper::getAString)//"a string"
                .when(!TestHelper.somethingIsTrue())
                .then(TestHelper::printFirstChar)
                .orElse(doNothing());

        //then
        Assert.assertTrue(outContent.toString().contains(""));
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void given_whenFalse_thenThrowRE() throws Exception {
        //when-then
        given(TestHelper::getAString)
                .when(!TestHelper.somethingIsTrue())
                .then(TestHelper::printFirstChar)
                .orElseThrow(RuntimeException::new);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void given_whenFalse_thenThrowRE_asSimpleType() throws Exception {
        //when-then
        given(TestHelper::getAString)
                .when(!TestHelper.somethingIsTrue())
                .then(TestHelper::printFirstChar)
                .orElseThrowE(new RuntimeException());
    }


}